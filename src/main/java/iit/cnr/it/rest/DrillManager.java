package iit.cnr.it.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Class used to manage the connection to Apache Drill database
 * 
 * @author Giacomo Giorgi
 *
 */

public class DrillManager {

  Connection connection;
  Statement statement;

  public DrillManager() {

  }

  /**
   * The method is used to connect to the Apache Drill database using the JDBC
   * driver and create a new statement in order to run queries
   * 
   * @param ip_add
   * @return boolean value that identify if the connection has been established
   */
  public boolean connect(String ip_add) {
    boolean connected = true;
    try {
      // load the JDBC driver
      Class.forName("org.apache.drill.jdbc.Driver");

      // Connect the Apache Drill
      Properties connectionProperties = new Properties();
      connectionProperties.put("autoReconnect", "true");
      connectionProperties.put("allowMultiQueries", "true");
      Connection connection = DriverManager.getConnection("jdbc:drill:drillbit="
          + ip_add, connectionProperties);
      this.connection = connection;
      this.statement = connection.createStatement();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      connected = false;
    } catch (SQLException e) {
      e.printStackTrace();
      connected = false;
    }
    return connected;
  }

  /**
   * The method is used to disconnect from the Apache Drill database
   * 
   * @return boolean value that identify if the disconnection has been done
   *         correctly
   */
  public boolean disconnect() {
    boolean disconnected = true;
    try {
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
      disconnected = false;
    }
    return disconnected;
  }

}
