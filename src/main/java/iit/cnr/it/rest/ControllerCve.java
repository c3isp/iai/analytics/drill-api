package iit.cnr.it.rest;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Logger;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import iit.cnr.it.drillCve.Cve;
import iit.cnr.it.drillCve.CveLatest;
import iit.cnr.it.drillCve.DrillCve;

/**
 * Rest controller used to manage the rest API related to vulnerabilities, CWE,
 * CAPEC and Attack signatures.
 **/

@RestController
@RequestMapping("/v1")
public class ControllerCve {

  // Statement variable
  private static Statement st;

  private final static Logger LOGGER = Logger.getLogger(ControllerCve.class
      .getName());

  // Ip address of the Apache Drill machine
  @Value("${drill.address}")
  private String drillIP;

  public ControllerCve() {
  }

  // ---------------CVE------------------
  @CrossOrigin(origins = "*")
  @RequestMapping(
      method = RequestMethod.GET,
      value = "getCveFromId")
  public String getCveFromId(@RequestParam("cve_id") String cve_id,
      @RequestParam("vulnerability_path") String cve_path) throws SQLException,
      ClassNotFoundException {

    if (cve_path.length() <= 1) {
      cve_path = "/home/iaic3isp/cti_data/vulnerabilities/cve/*/*";
    }

    DrillManager drill_manager = new DrillManager();
    drill_manager.connect(drillIP);
    st = drill_manager.statement;
    DrillCve drill_cve = new DrillCve(cve_path);

    ArrayList<Cve> cveList = drill_cve.getCveFromId(cve_id, st);
    drill_manager.disconnect();

    Gson gson = new Gson();
    String cveListJson = gson.toJson(cveList);
    return cveListJson;
  }

  @CrossOrigin(origins = "*")
  @RequestMapping(
      method = RequestMethod.GET,
      value = "getCveFromDate")
  public String getCveFromDate(@RequestParam("date_start") String date_start,
      @RequestParam("date_end") String date_end,
      @RequestParam("vulnerability_path") String cve_path) throws SQLException {

    if (cve_path.length() <= 1) {
      cve_path = "/home/iaic3isp/cti_data/vulnerabilities/cve/*/*";
    }

    DrillManager drill_manager = new DrillManager();
    drill_manager.connect(drillIP);
    DrillCve drill_cve = new DrillCve(cve_path);

    st = drill_manager.statement;
    ArrayList<Cve> cveList = drill_cve.getCVEFromDate(date_start, date_end, st);
    drill_manager.disconnect();

    Gson gson = new Gson();
    String cveListJson = gson.toJson(cveList);
    return cveListJson;
  }

  @CrossOrigin(origins = "*")
  @RequestMapping(
      method = RequestMethod.GET,
      value = "getLatestCves")
  public String getLatestCves(
      @RequestParam("vulnerability_path") String cve_path) throws SQLException {

    if (cve_path.length() <= 1) {
      cve_path = "/home/iaic3isp/cti_data/vulnerabilities/cve/*/*";
    }

    DrillManager drill_manager = new DrillManager();
    drill_manager.connect(drillIP);
    st = drill_manager.statement;
    DrillCve drill_cve = new DrillCve(cve_path);
    ArrayList<CveLatest> cveList = drill_cve.getLatestCVE(st);
    drill_manager.disconnect();

    Gson gson = new Gson();
    String cveListJson = gson.toJson(cveList);
    return cveListJson;
  }

  @CrossOrigin(origins = "*")
  @RequestMapping(
      method = RequestMethod.GET,
      value = "getCveFromKeyword")
  public String getCveFromKeyword(@RequestParam("keyword") String keyword,
      @RequestParam("vulnerability_path") String cve_path) throws SQLException {

    if (cve_path.length() <= 1) {
      cve_path = "/home/iaic3isp/cti_data/vulnerabilities/cve/*/*";
    }

    DrillManager drill_manager = new DrillManager();
    drill_manager.connect(drillIP);
    st = drill_manager.statement;
    LOGGER.info("IP: " + drillIP + "\t" + (drill_manager.statement == null));
    DrillCve drill_cve = new DrillCve(cve_path);

    ArrayList<Cve> cveList = drill_cve.getCVEFromKeyword(keyword, st);
    drill_manager.disconnect();

    Gson gson = new Gson();
    String cveListJson = gson.toJson(cveList);
    return cveListJson;
  }

}
