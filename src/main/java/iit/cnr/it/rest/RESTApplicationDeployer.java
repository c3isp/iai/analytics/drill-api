package iit.cnr.it.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This is the deployer for the rest api provided by the usage control framework
 * 
 * @author Antonio La Marra
 *
 */
@SpringBootApplication
@EnableSwagger2
@EnableAsync
public class RESTApplicationDeployer extends SpringBootServletInitializer {

  @Value("${security.activation.status}")
  private boolean securityActivationStatus;

  /**
   * Spring boot method for configuring the current application, right now it
   * automatically scans for interfaces annotated via spring boot methods in all
   * sub classes
   */
  @Override
  protected SpringApplicationBuilder configure(
      SpringApplicationBuilder application) {
    return application.sources(RESTApplicationDeployer.class);
  }

  /**
   * Docket is a SwaggerUI configuration component, in particular specifies to
   * use the V2.0 (SWAGGER_2) of swagger generated interfaces it also tells to
   * include only paths that are under /v1/. If other rest interfaces are added
   * with different base path, they won't be included this path selector can be
   * removed if all interfaces should be documented.
   */
  @Bean
  public Docket documentation() {
    Docket docket = new Docket(DocumentationType.SWAGGER_2);
    docket.apiInfo(metadata());
    if (!securityActivationStatus) {
      return docket
          .select()
          .paths(PathSelectors.regex("/v1/.*"))
          .build();
    } else {
      return docket
          // .securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new
          // ApiKey("mykey", "api_key", "header"))))
          .securitySchemes(new ArrayList<BasicAuth>(Arrays.asList(new BasicAuth(
              "basicAuth"))))
          .securityContexts(new ArrayList<SecurityContext>(Arrays.asList(
              securityContext())))
          .select()
          .paths(PathSelectors.regex("/v1/.*"))
          .build();
    }
  }

  /**
   * Selector for the paths this security context applies to ("/v1/.*")
   */
  private SecurityContext securityContext() {
    return SecurityContext.builder()
        .securityReferences(defaultAuth()).forPaths(PathSelectors.regex(
            "/v1/.*"))
        .build();
  }

  /**
   * Here we use the same key defined in the security scheme (basicAuth)
   */
  List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global",
        "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    // return new ArrayList<SecurityReference>(Arrays.asList(new
    // SecurityReference("mykey", authorizationScopes)));
    return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference(
        "basicAuth", authorizationScopes)));
  }

  /**
   * it just tells swagger that no special configuration are requested
   */
  @Bean
  public UiConfiguration uiConfig() {
    return new UiConfiguration(
        "validatorUrl" // set url
    );
  }

  /**
   * the metadata are information visualized in the /basepath/swagger-ui.html
   * interface, only for documentation
   */
  private ApiInfo metadata() {
    return new ApiInfoBuilder()
        .title("REST API for Apache DRILL")
        .description("API for CRUD operation template")
        .version("1.0")
        .contact(new Contact("Giacomo Giorgi", "",
            "giacomo.giorgi@iit.cnr.it"))
        .build();
  }

  /**
   * boot out SpringBoot application
   */
  public static void main(String[] args) {
    SpringApplication.run(RESTApplicationDeployer.class, args);
  }

  @Bean
  public Executor asyncExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(10);
    executor.setMaxPoolSize(20);
    executor.setQueueCapacity(500);
    executor.setThreadNamePrefix("IAI-API ");
    executor.initialize();
    return executor;
  }

}
