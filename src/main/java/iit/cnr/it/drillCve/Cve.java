package iit.cnr.it.drillCve;

import java.util.ArrayList;

public class Cve {

	String impact_ava = "";
	String impact_conf = "";
	String impact_inte = "";
	String cwe = "";
	String cvss_time = "";
	String cve_id = "";
	String summary = "";
	String published = "";
	ArrayList<String> cpe;
	ArrayList<String> cpe_2;
	String access_auth = "";
	String access_vec = "";
	String access_comp = "";
	String cvss = "";
	
	public Cve(){
		cpe = new ArrayList<String>();
		cpe_2 = new ArrayList<String>();
		cwe = "";
	}
	
	public void setCwe(String cwe){
		this.cwe = cwe;

	}

	
	public void setPublished(String published){
		this.published = published;
	}
	
	public void setCvss_time(String cvss_time){
		this.cvss_time = cvss_time;

	}
	
	public void setCve_id(String cve_id){
		this.cve_id = cve_id;

	}
	
	public void setAccess_auth(String access_auth){
		this.access_auth = access_auth;

	}
	
	public void setAccess_vec(String access_vec){
		this.access_vec = access_vec;

	}
	
	public void setAccess_comp(String access_comp){
		this.access_comp = access_comp;

	}
	
	public void setCvss(String cvss){
		this.cvss = cvss;

	}
	
	public void setSummary(String summary){
		this.summary = summary;

	}
	
	public void setImpact_ava(String impact_ava){
		this.impact_ava = impact_ava;

	}
	
	public void setImpact_conf(String impact_conf){
		this.impact_conf = impact_conf;

	}
	
	public void setImpact_inte(String impact_inte){
		this.impact_inte = impact_inte;

	}
	
	public void setCpe(ArrayList<String> cpeList) {
		this.cpe = cpeList;
	}
	
	public void setCpe_2(ArrayList<String> cpeList) {
		this.cpe_2 = cpeList;
	}
	
	
	
	
	public String getImpact_ava(){
		return this.impact_ava;
	}
	
	public String getImpact_conf(){
		return this.impact_conf;
	}
	
	public String getImpact_inte(){
		return this.impact_inte;
	}

	
	public ArrayList<String> getCpe() {
		return cpe;
	}
	
	public ArrayList<String> getCpe_2() {
		return cpe_2;
	}
	
	public String getCwe(){
		return cwe;

	}
	
	public String getCvss_time(){
		return cvss_time;

	}
	
	public String getCve_id(){
		return cve_id;

	}
	
	public String getAccess_auth(){
		return access_auth;

	}
	
	public String getAccess_vec(){
		return access_vec;

	}
	
	public String getAccess_comp(){
		return access_comp;

	}
	
	public String getCvss(){
		return cvss;

	}
	
	public String getSummary(){
		return summary;

	}
	
	public String getPublished(){
		return published;
	}
	
	
}
