package iit.cnr.it.drillCve;

import java.util.ArrayList;

public class AttackSignature {

  String description = "";
  String summary = "";
  String malwareName = "";
  String attackName = "";
  String severity = "";
  String informations = "";
  ArrayList<String> affected;
  String cve = "";

  public AttackSignature() {
    this.affected = new ArrayList<String>();
  }

  public void setCve(String cve) {
    this.cve = cve;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public void setInformations(String informations) {
    this.informations = informations;
  }

  public void setAffected(ArrayList<String> affectedList) {
    this.affected = affectedList;
  }

  public String getDescription() {
    return this.description;
  }

  public String getSummary() {
    return this.summary;
  }

  public String getSeverity() {
    return this.severity;
  }

  public String getInformations() {
    return this.informations;
  }

  public ArrayList<String> getAffected() {
    return this.affected;
  }

  public String getCve() {
    return this.cve;
  }

  public String getMalwareName() {
    return malwareName;
  }

  public void setMalwareName(String malwareName) {
    this.malwareName = malwareName;
  }

  public String getAttackName() {
    return attackName;
  }

  public void setAttackName(String attackName) {
    this.attackName = attackName;
  }

}
