package iit.cnr.it.drillCve;

import java.util.ArrayList;

public class Capec {
	String id = "";
	String name = "";
	String prerequisites = "";
	String solutions = "";
	String summary = "";
	ArrayList<String> related_weakness;



	
	public Capec(){
		related_weakness = new ArrayList<String>();
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	
	public void setName(String name){
		this.name = name;

	}
	
	public void setWeakness(String weakness){
		this.related_weakness.add(weakness);

	}
	
	public void setPrerequisites(String prerequisites){
		this.prerequisites = prerequisites;

	}	
	
	public void setSolutions(String solutions){
		this.solutions = solutions;
	}
	
	public void setSummary(String summary){
		this.summary = summary;
		
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;

	}
	
	public ArrayList<String> getWeakness(){
		return this.related_weakness;

	}
	
	public String getPrerequisites(){
		return this.prerequisites;
	}
	
	public String getSolutions(){
		return this.solutions;
	}
	
	public String getSummary(){
		return this.summary;
	}
	
}
