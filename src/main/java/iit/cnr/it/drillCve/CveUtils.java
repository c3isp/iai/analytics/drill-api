package iit.cnr.it.drillCve;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class CveUtils {

  public CveUtils() {

  }

  public static ArrayList<Cve> createCveList(ResultSet rs) throws SQLException {
    ArrayList<Cve> cveList = new ArrayList<Cve>();

    while (!rs.isClosed() && rs.next()) {

      Cve cve_obj = new Cve();
      if (rs.getString("vulnerable_configuration") != null) {

        String cpes = rs.getString("vulnerable_configuration").replace("[", "");
        cpes = cpes.replace("]", "");
        cpes = cpes.replace("\"", "");
        ArrayList<String> cpeList;
        if (cpes.contains(",")) {
          cpeList = new ArrayList<String>(Arrays.asList(cpes.split(",")));
        } else {
          cpeList = new ArrayList<String>(Arrays.asList(cpes));
        }

        cve_obj.setCpe(cpeList);
      } else
        cve_obj.setCpe(null);

      if (rs.getString("impact_ava") != null)
        cve_obj.setImpact_ava(rs.getString("impact_ava"));
      else
        cve_obj.setImpact_ava("");

      if (rs.getString("impact_conf") != null)
        cve_obj.setImpact_conf(rs.getString("impact_conf"));
      else
        cve_obj.setImpact_conf("");

      if (rs.getString("impact_inte") != null)
        cve_obj.setImpact_inte(rs.getString("impact_inte"));
      else
        cve_obj.setImpact_inte("");

      if (rs.getString("cwe") != null)
        cve_obj.setCwe(rs.getString("cwe"));
      else
        cve_obj.setCwe("");

      if (rs.getString("cvss_time") != null)
        cve_obj.setCvss_time(rs.getString("cvss_time"));
      else
        cve_obj.setCvss_time("");

      if (rs.getString("summary") != null)
        cve_obj.setSummary(rs.getString("summary"));
      else
        cve_obj.setSummary("");

      if (rs.getString("id") != null)
        cve_obj.setCve_id(rs.getString("id"));
      else
        cve_obj.setCve_id("");

      if (rs.getString("published") != null)
        cve_obj.setPublished(rs.getString("published"));
      else
        cve_obj.setPublished("");

      if (rs.getString("access_auth") != null)
        cve_obj.setAccess_auth(rs.getString("access_auth"));
      else
        cve_obj.setAccess_auth("");

      if (rs.getString("access_vec") != null)
        cve_obj.setAccess_vec(rs.getString("access_vec"));
      else
        cve_obj.setAccess_vec("");

      if (rs.getString("access_comp") != null)
        cve_obj.setAccess_comp(rs.getString("access_comp"));
      else
        cve_obj.setAccess_comp("");

      if (rs.getString("cvss") != null)
        cve_obj.setCvss(rs.getString("cvss"));
      else
        cve_obj.setCvss("");

      cveList.add(cve_obj);
    }
    return cveList;

  }

  public static ArrayList<CveLatest> createLatestCveList(ResultSet rs)
      throws SQLException {
    ArrayList<CveLatest> cveList = new ArrayList<CveLatest>();

    while (!rs.isClosed() && rs.next()) {
      CveLatest cve_obj = new CveLatest();

      if (rs.getString("cvss") != null)
        cve_obj.setCvss(rs.getString("cvss"));
      else
        cve_obj.setCvss("");

      if (rs.getString("summary") != null)
        cve_obj.setSummary(rs.getString("summary"));
      else
        cve_obj.setSummary("");

      if (rs.getString("id") != null)
        cve_obj.setCve_id(rs.getString("id"));
      else
        cve_obj.setCve_id("");

      if (rs.getString("published") != null)
        cve_obj.setPublished(rs.getString("published"));
      else
        cve_obj.setPublished("");

      cveList.add(cve_obj);
    }
    return cveList;

  }

  public static Cwe createCweObject(ResultSet rs) throws SQLException {
    Cwe cwe_obj = new Cwe();

    while (!rs.isClosed() && rs.next()) {
      cwe_obj.setStatus(rs.getString("status"));
      cwe_obj.setId(rs.getString("id"));
      cwe_obj.setName(rs.getString("name"));
      cwe_obj.setWeaknessabs(rs.getString("weaknessabs"));
      cwe_obj.setDescription(rs.getString("description_summary"));

    }
    return cwe_obj;
  }

  public static ArrayList<Capec> createCAPECList(ResultSet rs)
      throws SQLException {
    ArrayList<Capec> capecList = new ArrayList<Capec>();
    while (!rs.isClosed() && rs.next()) {
      Capec capec_obj = new Capec();

      capec_obj.setId(rs.getString("id"));
      ;
      capec_obj.setName(rs.getString("name"));
      capec_obj.setPrerequisites(rs.getString("prerequisites"));
      capec_obj.setSolutions(rs.getString("solutions"));
      capec_obj.setSummary(rs.getString("summary"));
      capec_obj.setWeakness(rs.getString("weakness"));

      capecList.add(capec_obj);

    }

    return capecList;
  }

  public static ArrayList<AttackSignature> createAttackList(ResultSet rs)
      throws SQLException {
    ArrayList<AttackSignature> attackList = new ArrayList<AttackSignature>();

    while (!rs.isClosed() && rs.next()) {

      AttackSignature attack_obj = new AttackSignature();

      if (rs.getString("Affected") != null) {

        String affected = rs.getString("Affected").replace("[", "");
        affected = affected.replace("]", "");
        affected = affected.replace("\"", "");
        ArrayList<String> affectedList;
        if (affected.contains(",")) {
          affectedList = new ArrayList<String>(Arrays.asList(affected.split(
              ",")));
        } else {
          affectedList = new ArrayList<String>(Arrays.asList(affected));
        }

        attack_obj.setAffected(affectedList);
      } else
        attack_obj.setAffected(null);

      attack_obj.setDescription(rs.getString("Description"));
      ;
      attack_obj.setSummary(rs.getString("Summary"));
      attack_obj.setMalwareName(rs.getString("malware"));
      attack_obj.setAttackName(rs.getString("attack_name"));
      attack_obj.setSeverity(rs.getString("Severity"));
      attack_obj.setInformations(rs.getString("informations"));
      attack_obj.setCve(rs.getString("cve"));
      attackList.add(attack_obj);

    }

    return attackList;
  }

}
