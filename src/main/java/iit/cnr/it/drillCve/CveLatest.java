package iit.cnr.it.drillCve;


public class CveLatest{

	
	String cve_id = "";
	String summary = "";
	String published = "";
	String cvss = "";
	
	public CveLatest(){
		
	}
	
	public void setPublished(String published){
		this.published = published;
	}

	
	public void setCve_id(String cve_id){
		this.cve_id = cve_id;

	}
	
	
	public void setCvss(String cvss){
		this.cvss = cvss;

	}
	
	public void setSummary(String summary){
		this.summary = summary;

	}
	
	
	public String getCve_id(){
		return cve_id;

	}
	
	public String getCvss(){
		return cvss;

	}
	
	public String getSummary(){
		return summary;

	}
	
	public String getPublished(){
		return published;
	}
	
}
