package iit.cnr.it.drillCve;

public class Cwe {
	
	String id = "";
	String status = "";
	String name = "";
	String weaknessabs = "";
	String description_summary = "";


	
	public Cwe(){
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	
	public void setStatus(String status){
		this.status = status;

	}
	
	public void setName(String name){
		this.name = name;

	}
	
	public void setWeaknessabs(String weaknessabs){
		this.weaknessabs = weaknessabs;

	}
	
	public void setDescription(String description_summary){
		this.description_summary = description_summary;

	}
	
	
	public String getId(){
		return this.id;
	}
	
	public String getStatus(){
		return this.status;

	}
	
	public String getName(){
		return this.name;

	}
	
	public String getWeaknessabs(){
		return this.weaknessabs;

	}
	
	public String getDescription_summary(){
		return this.description_summary;

	}
	
	
	
	
}