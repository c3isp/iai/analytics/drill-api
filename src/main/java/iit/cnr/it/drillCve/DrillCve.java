package iit.cnr.it.drillCve;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Class that manage all the queries related to the CVEs tables
 * 
 * @author Giacomo Giorgi
 *
 */
public class DrillCve {

  private String CVE_PATH;

  public DrillCve(String cve_path) {
    this.CVE_PATH = cve_path;
  }

  /**
   * Method that executes the query on the Cve table given a cve identifier
   * 
   * @param cve_id
   *          the cve identifier to search the vulnerability informations
   * @param st
   *          the statement to execute the query
   * @return an arrayList of Cve object containing the result of the query
   * @throws SQLException
   */
  public ArrayList<Cve> getCveFromId(String cve_id, Statement st)
      throws SQLException {
    ArrayList<Cve> cveList = new ArrayList<Cve>();
    String query = "SELECT vulnerable_configuration, vulnerable_configuration_cpe_2_2, cve.impact.availability as impact_ava, cve.impact.confidentiality as impact_conf, cve.impact.integrity as impact_inte, cwe, cvss_time, id, summary, cve.access.authentication as access_auth, cve.access.vector as access_vec, cve.access.complexity as access_comp, cvss, published FROM dfs.`"
        + CVE_PATH + "` cve WHERE id='" + cve_id + "'";
    ResultSet rs = st.executeQuery(query);
    cveList = CveUtils.createCveList(rs);
    return cveList;
  }

  /**
   * Method that executes the query on the Cve table given a date interval
   * 
   * @param dateStart
   *          begin date (YYYY-MM-DD)
   * @param dateEnd
   *          end date (YYYY-MM-DD)
   * @param offset
   * @param st
   *          the statement to execute the query
   * @return an arrayList of Cve object containing the result of the query
   * @throws SQLException
   */
  public ArrayList<Cve> getCVEFromDate(String dateStart, String dateEnd,
      Statement st) throws SQLException {

    ArrayList<Cve> cveList = new ArrayList<Cve>();
    ResultSet rs = st.executeQuery(
        "SELECT vulnerable_configuration, vulnerable_configuration_cpe_2_2,cve.impact.availability as impact_ava, cve.impact.confidentiality as impact_conf, cve.impact.integrity as impact_inte, cwe, cvss_time, id, summary, published, cve.access.authentication as access_auth, cve.access.vector as access_vec, cve.access.complexity as access_comp, cvss FROM dfs.`"
            + CVE_PATH + "` cve WHERE published BETWEEN '" + dateStart
            + "' AND '" + dateEnd + "' ORDER BY(published) DESC LIMIT 100");
    cveList = CveUtils.createCveList(rs);
    return cveList;
  }

  /**
   * Method that execute the query on the Cve tables looking for the last 20
   * result ordered by date
   * 
   * @param st
   * @return
   * @throws SQLException
   */
  public ArrayList<CveLatest> getLatestCVE(Statement st) throws SQLException {
    ArrayList<CveLatest> cveList = new ArrayList<CveLatest>();
    ResultSet rs = st.executeQuery(
        "SELECT id, summary, published, cvss FROM dfs.`" + CVE_PATH
            + "` cve ORDER BY(published) DESC LIMIT 20");
    cveList = CveUtils.createLatestCveList(rs);
    return cveList;
  }

  /**
   * 
   * @param keyword
   * @param offset
   * @param st
   * @return
   * @throws SQLException
   */
  public ArrayList<Cve> getCVEFromKeyword(String keyword, Statement st)
      throws SQLException {
    ArrayList<Cve> cveList = new ArrayList<Cve>();
    ResultSet rs = st.executeQuery(
        "SELECT vulnerable_configuration, vulnerable_configuration_cpe_2_2, cve.impact.availability as impact_ava, cve.impact.confidentiality as impact_conf, cve.impact.integrity as impact_inte, cwe, cvss_time, id, summary, published, cve.access.authentication as access_auth, cve.access.vector as access_vec, cve.access.complexity as access_comp, cvss FROM dfs.`"
            + CVE_PATH + "` cve WHERE lower(summary) LIKE '%" + keyword
                .toLowerCase() + "%' ORDER BY(published) DESC LIMIT 100");
    cveList = CveUtils.createCveList(rs);
    return cveList;
  }

}
